var mongoose = require('mongoose'),
    bcrypt   = require('bcrypt');

var UserSchema = mongoose.Schema({
    username : { 
        type: String, 
        unique: true 
    }, 
    email: { 
        type: String, 
        unique: true 
    },
    local: {
        password : String
    },
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

UserSchema.statics.findByEmailOrQuery = function(email,query,callback) {
    this.findOne( { $or:[ {email:email}, query ]}, callback);
};

module.exports = mongoose.model('User', UserSchema);