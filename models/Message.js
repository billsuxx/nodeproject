var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MessageSchema = new Schema({
    user : { 
    	type: Schema.ObjectId,
    	ref: 'User'
    },
    text: { 
    	type: String, 
    	unique: false 
    },
    datetime: { 
    	type: String, 
    	unique: false 
    }
});

module.exports = mongoose.model('Message', MessageSchema);