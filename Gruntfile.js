'use strict';

var request = require('request');

/**
 * TODO:
 * concat, min, ugly
 *
 */
module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);

    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    var reloadPort = 35729;

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        //test pina
        develop: {
            server: {
                file: 'app.js',
            },
        },
        // https://github.com/ChrisWren/grunt-nodemon 
        // options nevek valtoztak 0.2.0-ban!!!
        nodemon: {
            dev: { 
                script : 'app.js',
                options: {
                    ext: 'js,css,less,html',
                    ignore: ['node_modules/**'],
                    watch: ['routes', 'source/**'],
                    delay: 1,
                    legacyWatch: true,
                    cwd: __dirname,
                    env: {
                        NODE_ENV: 'development',
                        PORT: '3000'
                    }
                }
            }
        },
        /* css to less */
        less: {
            dev: {
                options: {
                    compress: false,
                    yuicompress: false,
                    optimization: 2
                },
                files: {
                    "public/css/style.css": "source/css/main/init.less",
                    "public/css/bootstrap.css": "source/css/bootstrap/bootstrap_chat.less"
                }
            }
        },

        concat: {
            options: {
                separator: ';',
            },
            /*
            bootstrap: {
                src: [
                    'source/js/bootstrap/affix.js',
                    'source/js/bootstrap/alert.js',
                    'source/js/bootstrap/affix.js',
                    'source/js/bootstrap/button.js',
                    'source/js/bootstrap/carousel.js',
                    'source/js/bootstrap/modal.js',
                    'source/js/bootstrap/collapse.js',
                    'source/js/bootstrap/dropdown.js',
                    'source/js/bootstrap/scrollspy.js',
                    'source/js/bootstrap/tab.js',
                    'source/js/bootstrap/tooltip.js',
                    'source/js/bootstrap/popover.js',
                    'source/js/bootstrap/transition.js',

                    ],
                dest: 'public/js/bootstrap.js',
            },
            */
            main: {
                src: 'source/js/main/*.js',
                dest: 'public/js/portal.js',
            }
        },
         
        /* valtozasok figyelese */
        watch: {
            files: [
                'app.js',
                'views/**',
                'source/css/main/*.less',
                'source/js/main/*.js'
            ],
            tasks: ['less', 'concat'],
            options: {
                nospawn: true,
                livereload: reloadPort
            }
        },
        concurrent: {
            dev: {
                tasks: ['nodemon:dev', 'less', 'concat', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        }

    });

    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-nodemon');

    grunt.registerTask('dev', ['concurrent:dev']);

};
