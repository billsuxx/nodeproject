
$(document).ready(function(){

	var chatApp = {

		/**
		 * Uzenet kuldo form.
		 */
		chatForm: $('#chat-form'),

		/**
		 * Uzenetek megjelenito terulet
		 */
		chatArea: $('chat-area'),

		/**
		 * App inicializalas
		 * 
		 * @return {[type]}
		 */
		init: function() {

			this.bindEvents();

		},

		/**
		 * Esemenyek felhuzasa a megfelelo mezokre
		 * 
		 * @return {[type]}
		 */
		bindEvents: function() {

			$('#chat-send').click($.proxy(function(ev){
				ev.preventDefault(); 
				this.sendMessage();
			},this));

		},

		/**
		 * Uzenet elkuldese.
		 * 
		 * @return {[type]}
		 */
		sendMessage: function() {

			var formData = this.chatForm.serialize();

			$.ajax('chat',{
				type: 'post',
				data: formData,
				success: function(response) {
					console.log(response);

					if (response.result == true) {

						$('#chat-form').find('#message').val('');

					} 

				},
				error: function(response) {
					console.log(response);
				}
			});

		},

		/**
		 * Uzenetek betoltese a serverrol
		 * @return {[type]}
		 */
		pullMessages: function(){
			$.ajax('load-chat', {
				type: 'get',
				success: function(response) {

					$.each(response.messages,function(i,item) {
						//<p> [{{datetime}}] <strong>&lt;{{user.username}}&gt;:</strong> {{text}}</p>
					})


				}
			})
		}

	};

	chatApp.init();

	
});