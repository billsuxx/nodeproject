var LocalStrategy = require('passport-local').Strategy;

/**
 * Passport megoldasa innen
 * @url: https://github.com/fabiobozzo/
 * 
 * @param {Object} password Passport
 * @param {Object} User     Mongoose UserSchema
 */
module.exports = function(passport,User) {

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // passport signup
    passport.use('local-signup', new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true 
        },function(req, email, password, done) {
            process.nextTick(function() {
                User.findOne({ 'email' :  email }, function(err, user) {
                    
                    if (err) return done(err);

                    if (user) {
                        return done(null, false, req.flash('errorMessage', 'Email already in use!'));
                    } else {
                        var newUser             = new User();
                        newUser.username        = req.body.username || '';
                        newUser.email           = email;
                        newUser.local.password  = newUser.generateHash(password);

                        newUser.save(function(err) {
                            if (err) throw err;
                            return done(null, newUser);
                        });
                    }

                });    
            });
        })
    );

    // passwport login
    passport.use('local-login', new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true 
        },
        function(req, email, password, done) { 
            User.findOne({ 'email' :  email }, function(err, user) {

                if (err) return done(err);

                if (!user) {
                    return done(null, false, req.flash('errorMessage', 'User and password error.')); 
                }

                if (!user.validPassword(password)) {
                    return done(null, false, req.flash('errorMessage', 'User and password error.')); 
                }
                
                req.session.user = user;
                return done(null, user);
            });
        })
    );

};