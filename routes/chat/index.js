var mongoose = require('mongoose');

/**
 * Chathez kotodo routeok
 */
module.exports = function(app,Message) {

    app.get('/chat', function(req, res) {
        if (req.session.user) {

            Message.find( {},  function(err, messages) {
                Message.populate(messages, {path: "user"}, function(err, messages) {

                    if (err) return done(err);

                    if (messages) {
                        res.render('chat.html', {   
                            title: 'Chat',
                            session: req.session,
                            messages: messages
                        });
                     }

                })
            });


        } else {
            req.flash('errorMessage', 'Access denied!');
            res.redirect('/login');
        }
    });

    app.post('/chat',  function(req, res, done) {
        var now               = new Date();
        var m                 = '0' + (now.getMonth()+1);
        var d                 = '0' + now.getDate();
        var newMessage        = new Message();
        newMessage.user         = req.session.user._id || '0';
        newMessage.text       = req.body.message;
        newMessage.datetime   = now.getFullYear() + '.' + m.slice(-2) + '.' + d.slice(-2) + ' ' + 
        now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();

        newMessage.save(function(err) {
            if (err) throw err;
            return done(null, newMessage);
        });

        res.json({
            result: true
        });

        //       res.redirect('/chat?ok'); 
    });

};
