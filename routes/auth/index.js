module.exports = function(app,passport) {

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/chat', 
        failureRedirect : '/login', 
        failureFlash : true 
    }));

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/signup-success', 
        failureRedirect : '/signup', 
        failureFlash : true 
    }));

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/login#login');
    });
    
};