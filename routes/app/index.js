/**
 * Nem az autentikaciohoz (passport) kotheto routok
 */
module.exports = function(app) {

    app.get('/', function(req, res) {
        res.render('index.html', {
            title: 'Home',
            session: req.session
        });
    });

    app.get('/profil', function(req, res) {
        if (req.session.user) {
            console.log(req.session.user);
            res.render('profil.html', { 
                title: 'Profil',
                session: req.session
            });
        } 
        else {
            req.flash('errorMessage', 'Access denied!');
            res.redirect('/login');
        }
    });

    app.get('/login', function(req,res){
        res.render('login.html', { 
            title: 'Login',
            message: req.flash('errorMessage') 
        });
    });

    app.get('/signup', function(req,res){
        res.render('signup.html', { 
            title: 'Signup',
            message: req.flash('errorMessage') 
        });
    });

    app.get('/signup-success', function(req,res){
        res.render('signup_success.html', { 
        });
    });

    app.get('/about', function(req,res){
        res.render('about.html', { 
            session: req.session
        });
    });

};
