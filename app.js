/**
 * Module dependencies.
 */
var express        = require('express'), 
    path           = require('path'),
    hbs            = require('express-hbs'),
    passport       = require('passport'),
    flash          = require('connect-flash'),
    db             = require('./lib/db.js'),
    User           = require('./models/User.js'),
    Message        = require('./models/Message.js'),
    LocalStrategy  = require('passport-local').Strategy,
    passportConfig = require('./lib/passport.js'),
    appRoutes      = require('./routes/app/'),
    chatRoutes     = require('./routes/chat/');
    authRoutes     = require('./routes/auth/');

var app = express();

app.configure(function(){
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');    
    app.set('view engine', hbs.express3);

    app.engine('html', hbs.express3({  
        extname      : '.html', 
        defaultLayout: __dirname + '/views/layouts/main.html',
        partialsDir: __dirname + '/views/partials',
        layoutsDir: __dirname + '/views/layouts'
    }));
    
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());

    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.cookieParser());
    app.use(express.session({secret: "This is a secret"}));

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(flash()); 
    app.use(app.router);
    
});

appRoutes(app); 
authRoutes(app,passport); 
chatRoutes(app,Message); 
passportConfig(passport,User);

app.configure('development', function(){
    app.use(express.errorHandler());
});


/* start app */
app.listen(app.get('port'),function(){
    console.log("app.js PORT: " + app.get('port'));
    console.log('app.js NODE_ENV: ' + app.get('env'));
});

process.on('uncaughtException', function (err) {
    console.error('uncaughtException:', err.message);
    console.error(err.stack);
    process.exit(1);
});

